"""
 fscan_coherenceFromSFTs is part of fscan package
 Last Modification: 17 November 2021
 Version: Beta
 Author's Email:
 Modified by: Sudhagar Suyamprakasam
 Contributor(s): Kara Merfeld, John Whelan
"""

import sys
import os
import numpy as np
import struct
import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams['text.usetex'] = False
matplotlib.use('Agg')

"""
# TODO: check crc64 checksum
"""


def parseSFT(SFTinput):
    """
    This function parses an SFT (short fourier transform) formatted
    according to the SFT specification version 2,
    https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=T040164
    It takes as input either an open (for binary reading) file object
    or a string specifying the path to the SFT file.  It returns the
    SFT data and metadata in a dictionary.  If more than one SFT is in
    a file, it returns a tuple of dictionaries.

    Parameters
    ----------
    SFTinput: String
              Path to SFT file directory or SFT file.

    Return
    ------
    sfts: dict
         sft data and its parameters
    """
    SFTfile = open(SFTinput, 'rb')

    # Loop over SFTs in the file
    while True:

        header_data = SFTfile.read(48)

        # We've reached the end
        if len(header_data) == 0:
            break

        # Parse header (48 bytes)
        (version,
         gps_sec,
         gps_nsec,
         tbase,
         first_frequency_index,
         nsamples,
         crc64,
         detector,
         padding,
         comment_length
         ) = struct.unpack('diidiiQ2s2si', header_data)

        # Check version (and endianness)
        if version != 2.0:
            raise ValueError('Can only parse SFTs of version 2, \
                             not %f' % version)

        # Read comment
        comment = struct.unpack(('%ds' % comment_length),
                                SFTfile.read(comment_length))[0]

        # Read data
        data_stream = struct.unpack(('%df' % nsamples*2),
                                    SFTfile.read(nsamples*8))

        # Pack real values into complex array
        data = np.array(data_stream[0::2]) + 1.0j * np.array(data_stream[1::2])

        sft = {
            'version': version,
            'gps_sec': gps_sec,
            'gps_nsec': gps_nsec,
            'tbase': tbase,
            'first_frequency_index': first_frequency_index,
            'nsamples': nsamples,
            'crc64': crc64,
            'detector': detector,
            'padding': padding,
            'comment_length': comment_length,
            'comment': comment,
            'data': data
            }

    return sft


def coherenceFromSFTs(pathToSFTsChanA, pathToSFTsChanB, subBand=None,
                      timeStamp=None):
    """
    The function that generates the coherence

    Parameters
    ----------

    pathToSFTsChanA: string
                      Path to Channel A SFT file directory or SFT file.

    pathToSFTsChanB: string
                      Path to Channel B SFT file directory or SFT file.

    subBand: float
             Sub Band coherence plot, Default 100.

    timeStamp: string


    Return
    ------
    None

    """
    # Check SFT path or file

    if (pathToSFTsChanA[-1] != '/') and \
       (pathToSFTsChanA.endswith('.sft') is False):
        pathToSFTsChanA = pathToSFTsChanA + '/'

    if pathToSFTsChanA[-1] == '/':
        ListA = sorted(os.listdir(pathToSFTsChanA))
    elif pathToSFTsChanA.endswith('.sft'):
        # Split path and file for specific sft channel A file
        split_slashA = pathToSFTsChanA.split('/')
        rejoin_pathA = '/'.join(split_slashA[0:len(split_slashA)-1])
        pathToSFTsChanA = rejoin_pathA+'/'
        ListA = [split_slashA[-1]]
    else:
        print('Channel A: Invalid data file or path')
        sys.exit()

    if (pathToSFTsChanB[-1] != '/') and \
       (pathToSFTsChanB.endswith('.sft') is False):
        pathToSFTsChanB = pathToSFTsChanB + '/'

    if pathToSFTsChanB[-1] == '/':
        ListB = sorted(os.listdir(pathToSFTsChanB))
    elif pathToSFTsChanB.endswith('.sft'):
        # Split path and file for specific sft channel B file
        split_slashB = pathToSFTsChanB.split('/')
        rejoin_pathB = '/'.join(split_slashB[0:len(split_slashB)-1])
        pathToSFTsChanB = rejoin_pathB+'/'
        ListB = [split_slashB[-1]]
    else:
        print('Channel B: Invalid data file or path')
        sys.exit()

    # Channel A and B starting sft file
    chanA_sft0 = parseSFT(pathToSFTsChanA+ListA[0])
    chanB_sft0 = parseSFT(pathToSFTsChanB+ListB[0])

    # Get Channel Names
    split_commentA = str(chanA_sft0['comment']).split(' ')
    split_commentB = str(chanB_sft0['comment']).split(' ')

    ChanAname = split_commentA[35]
    ChanBname = split_commentB[35]

    # Find Minimum frequency
    FminA = chanA_sft0['first_frequency_index'] / chanA_sft0['tbase']
    FminB = chanB_sft0['first_frequency_index'] / chanB_sft0['tbase']

    if FminA <= FminB:
        Fmin = FminB
    else:
        Fmin = FminA

    # Find Maximum frequency
    FmaxA = FminA + (chanA_sft0['nsamples']) / chanA_sft0['tbase']
    FmaxB = FminB + (chanB_sft0['nsamples']) / chanB_sft0['tbase']

    if FmaxA <= FmaxB:
        Fmax = FmaxA
    else:
        Fmax = FmaxB

    # Start and end Minimum frequency indices
    KminA = int((Fmin-FminA) * chanA_sft0['tbase'])
    KminB = int((Fmin-FminB) * chanB_sft0['tbase'])

    # Start and end Maximum frequency indices
    KmaxA = int((Fmax-FminA) * chanA_sft0['tbase'])
    KmaxB = int((Fmax-FminB) * chanB_sft0['tbase'])

    # Initialize zero arrays
    zeroA = np.zeros(KmaxA)
    zeroB = np.zeros(KmaxB)

    ChannelA = zeroA[KminA:KmaxA]
    ChannelB = zeroB[KminB:KmaxB]

    numerator = zeroA[KminA:KmaxA]

    # Initialize number of averages
    nAverage = 0

    for Aind in range(0, len(ListA)):
        StartTimesA = parseSFT(pathToSFTsChanA+ListA[Aind])['gps_sec']
        for Bind in range(0, len(ListB)):
            StartTimesB = parseSFT(pathToSFTsChanB+ListB[Bind])['gps_sec']
            if StartTimesA == StartTimesB:
                ChanA_data = parseSFT(pathToSFTsChanA +
                                      ListA[Aind])['data'][KminA:KmaxA]
                ChanB_data = parseSFT(pathToSFTsChanB +
                                      ListB[Bind])['data'][KminB:KmaxB]

                ChannelA = ChannelA + ChanA_data * np.conj(ChanA_data)
                ChannelB = ChannelB + ChanB_data * np.conj(ChanB_data)
                numerator = numerator + ChanA_data * np.conj(ChanB_data)

                # Keep track of the number of averages
                nAverage += 1

    numerator = numerator * np.conj(numerator)

    # Coherene
    coherence = numerator / (ChannelA * ChannelB)
    coherence = np.real_if_close(coherence, tol=10)

    print('Coherence Completed; number of Averages = %d' % nAverage)

    # Frequency
    frequency = np.linspace(int(Fmin), int(Fmax), int(len(coherence)))

    # Start GPS Time
    startTime = parseSFT(pathToSFTsChanA+ListA[0])['gps_sec']
    startTime = str(startTime)

    # An array for the coherence output:
    Coh_Output = np.concatenate((frequency.reshape(-1, 1),
                                 coherence.reshape(-1, 1)), axis=1)

    if subBand is None:
        subBand = 100

    # Number of data points in the subbands
    subBand_npoints = int(subBand * chanA_sft0['tbase'])

    # Number of subbands n the given frequency range
    NsubBand = int(np.floor(len(frequency) / subBand_npoints))

    print('Generating plots and files')

    for nn in range(NsubBand):
        minFreqindex = int(nn * subBand * chanA_sft0['tbase'])
        maxFreqindex = int((nn + 1) * subBand * chanA_sft0['tbase'])

        # Filename:
        if timeStamp is None:
            filename = 'coh_%d_%d_%s_coherence_%s_and_%s' % \
                        (frequency[minFreqindex], frequency[maxFreqindex],
                         startTime, ChanAname, ChanBname)
        else:
            filename = 'coh_%d_%d_%s_coherence_%s_and_%s' % \
                        (frequency[minFreqindex], frequency[maxFreqindex],
                         timeStamp, ChanAname, ChanBname)

        np.savetxt(filename+'.txt',
                   Coh_Output[minFreqindex:maxFreqindex],
                   fmt=['%.6e', '%.4f'])

        ft = 18
        fig = plt.figure(figsize=(12, 12))
        ax1 = fig.add_subplot(111)
        ax1.plot(frequency[minFreqindex:maxFreqindex],
                 coherence[minFreqindex:maxFreqindex])
        ax1.set_title(ChanAname + ' and ' + ChanBname + '\n' + str(startTime) +
                      '; nAverage= ' + str(nAverage) + '\n',
                      fontsize=ft, fontweight='bold')
        ax1.set_xlabel('Frequency (Hz)', fontsize=ft, fontweight='bold')
        ax1.set_ylabel('Coherence', fontsize=ft, fontweight='bold')
        # ax1.set_ylim([0, 1])

        # Figure properties
        plt.rc('font', weight='bold')
        plt.tick_params(labelsize=ft)
        plt.rc('xtick', labelsize=ft)
        plt.rc('ytick', labelsize=ft)

        plt.savefig(filename+'.png')
        plt.close('all')

    print('Plots and files are generated')


# Main code starts from here

if len(sys.argv) < 3:
    print('Input arguments are missing.' +
          ' Find the coherence between SFTs in two specified directories.' +
          ' "Usage: %s <pathToSFTsChanA> <pathToSFTsChanB>' +
          ' [subBand] [timeStamp] % sys.argv[0]".' +
          ' The optional subBand is the band in Hz to output in each plot' +
          ' (Default is 100 Hz).' +
          ' The optional timeStamp is used in the name of the output files' +
          ' (Defaults to start time of the first SFT).')
    sys.exit()

pathToSFTsChanA = sys.argv[1]
pathToSFTsChanB = sys.argv[2]
subBand = 100
timeStamp = None

if len(sys.argv) >= 4:
    subBand = sys.argv[3]

if len(sys.argv) >= 5:
    timeStamp = sys.argv[4]

print('# --------------------- #')
print('Started Computing Coherence between two channels from SFT(s)')
coherenceFromSFTs(pathToSFTsChanA, pathToSFTsChanB, subBand, timeStamp)
print('Finished Computing Coherence between two channels from SFT(s)')
print('# --------------------- #')
